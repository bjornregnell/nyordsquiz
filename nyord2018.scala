case class Nyord(ord: String, förklaring: String, belägg: String, ref: String, kommentar: String){
  override def toString = s"""
  Nyord(ord="$ord",
    förklaring="$förklaring",
    belägg="$belägg",
    ref="$ref",
    kommentar="$kommentar"
  )
  """.stripMargin
}
object Nyord {
  def fromLine(xs: Seq[String]): Nyord = {
    def o(i: Int): String = xs.lift(i).getOrElse("")
    Nyord(o(0), o(1), o(2), o(3), o(4))
  }

  def fromFile(file: String): Seq[Nyord] = {
    scala.io.Source.fromFile(file, "UTF-8").getLines.mkString("\n")
      .split("#").map(_.trim).filterNot(_.isEmpty).toVector.map(_.split("\n").toVector)
      .map(Nyord.fromLine)
  }
}

object Latex {
  def document(body: String): String = s"""
    \\documentclass[12pt,a4paper]{article}
    \\usepackage{extsizes}
    \\usepackage[margin=0.7cm]{geometry}
    \\usepackage[utf8]{inputenc}
    \\usepackage[T1]{fontenc}
    \\usepackage{microtype} % Slightly tweak font spacing for aesthetics
    \\usepackage{droid}
    \\usepackage[swedish]{babel}


    \\begin{document}
    $body
    \\end{document}
  """

  def tabular(xss:Seq[Seq[String]], colSpec: String, heading: String = ""): String = s"""
    \\begin{tabular}{$colSpec}
    ${if (heading.nonEmpty) heading + "\\\\ \\hline " else ""}
    ${xss.map(xs => xs.mkString(" & ")).mkString("", "\\\\ \\hline \n", "\\\\\\hline")}
    \\end{tabular}
  """

}

object Pdf {
  def apply(inFile: String, workDir: String = "tex"): Unit = {
    val texFile = inFile + ".tex"
    val basePath = new java.io.File( "." ).getCanonicalPath()
    val workPath = s"$basePath/$workDir"
    val texPath = s"$workPath/$texFile"
    val logFile = texFile.replace(".tex", "-console.log")

    println(s" ******* Compiling $workDir/$texFile to pdf *******")
    val cmd = scala.sys.process.Process(
      Seq("pdflatex","-halt-on-error", texPath),
      new java.io.File(workPath)
    )
    val logPath =  s"""$workPath/$logFile"""
    if (new java.io.File(logPath).createNewFile) println(s"Created: $logPath")
    val exitValue = cmd.#>(new java.io.File(logPath)).run.exitValue
    if (exitValue != 0) {
      println("*** ############ ERROR LOG STARTS HERE ############### ***")
      scala.sys.process.Process(
        Seq("tail", "-40", logPath),
        new java.io.File(workDir)
      ).run
      sys.error(s"\n*** ERROR: pdflatex output in: $logPath")
    } else println(s"             Log file: $workDir/$logFile")
  }
}

object Main {
  implicit class StringSaver(s: String) {
    def toFile(fileName: String, enc: String = "UTF-8", suf: String = ".tex", wd: String = "tex/"): Unit = {
      val f = new java.io.File(wd + fileName + suf)
      val pw = new java.io.PrintWriter(f, enc)
      try pw.write(s) finally pw.close()
    }
  }

  scala.util.Random.setSeed(41)
  val nyordMap = Nyord.fromFile("nyord2018.md").map(n=> (n.ord, n)).toMap
  val keys = nyordMap.keySet.toSeq.sorted
  val keysShuffled = scala.util.Random.shuffle(keys)
  val nyordShuffled = scala.util.Random.shuffle(nyordMap.values)
  val nyordIndexed = nyordShuffled.zipWithIndex.toSeq
  val indexOfKey: Map[String, Int] = nyordIndexed.map{case (n,i) => (n.ord, i)}.toMap

  def main(args: Array[String]): Unit = {
    println(keys.mkString("\n"))
    println(keys.size)
    nyordIndexed.map { case (n,i) =>
      println(s"$i: ${n.förklaring}")
    }
    keysShuffled.foreach(println)
    keysShuffled.foreach(k => println(s"$k: ${indexOfKey(k)} ${nyordMap(k).förklaring}"))

    def table(isFacit: Boolean) = Latex.tabular(
      xss = (0 until keysShuffled.size).map(i => Seq(
            keysShuffled(i), if (isFacit) indexOfKey(keysShuffled(i)).toString else " ",
            nyordIndexed(i)._2.toString + ". "+  nyordIndexed(i)._1.förklaring
          )),
      colSpec = "r | c | p{14cm}",
      heading = "\\textit{nyord} & \\textit{index} & \\textit{förklaring}"
    )

    def nl(i: Int): String = "~\\\\~" * i

    def toFile(file: String, isFacit: Boolean): Unit = {
      Latex.document(
        "Namn:" + nl(2) + table(isFacit) + nl(2) + "Antal rätt: "
      ).toFile(file)
      Pdf(file)
    }

    toFile("nyordquiz", isFacit = false)
    toFile("nyordquiz-facit", isFacit = true)

  }

}
