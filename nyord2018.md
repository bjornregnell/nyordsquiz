#aquafaba
spad från baljväxter, oftast kikärtor, som kan användas i stället för äggvita i matlagning
Det finns en mängd olika sätt att ersätta ägg på veganskt vis. Ett lite oväntat sätt är att använda sig av spadet från kokta kikärtor på burk eller tetra. Spadet kallas även för aquafaba och kan vispas upp till ett fast skum precis som äggvitor.
Expressen 12 september 2018
Kommentar: Bildat till latinets aqua, 'vatten', och faba, 'böna'.

#beslutsblindhet
oförmåga att upptäcka manipulering av åsikter och ställningstaganden
Fenomenet att inte genomskåda manipulerade åsikter kallas beslutsblindhet. Det beskrevs av forskarna första gången 2005. Försökspersoner fick då välja vem de fann mest attraktiv av två personer på fotografier - som efter beslutet kastades om. Få deltagare upptäckte bluffen, och de argumenterade till synes rationellt för val de aldrig gjort.
Dagens Nyheter 7 september 2018

#bokashi
metod där matavfall komposteras genom fermentering
Bokashi är ett sätt att återvinna matavfall och omvandla det till jord och gödning utan kemikalier. Med hjälp av mikroorganismer omvandlas matavfallet till jordförbättring som kan användas i rabatten och kökslandet.
Nya Wermlands-Tidningen 17 augusti 2018
Kommentar: Inlånat från japanskan.

#cyberhygien
rutiner för sund it-säkerhet
I diskussionen framhölls vikten av god cyberhygien och mer och mer framstod behovet av en sorts cyber-Lubbe Nordström som tar itu med säkerhetsbristerna i hela samhället. Vi är duktiga på digitalisering men inte på IT-säkerhet.
Nya Wermlands-Tidningen 11 juli 2018

#digifysisk
som kombinerar digital närvaro och fysisk närvaro
Jobbar mycket med en kombination av digitala och fysiska besök, det som ofta kallas digifysisk vård.
Svenska Dagbladet 14 juli 2018
Kommentar: Teleskopord bildat till digital och fysisk.

#dm:a
skicka direktmeddelande på exempelvis Instagram, Facebook och Twitter
Blev skitskraj när en följare dm:ade att hennes brorsas son inte fått rätt diagnos och blivit blind på ena ögat.
Personlig blogg 26 september 2018
Kommentar: Inlånat från engelskans dm, som är en initialförkortning av direct message.

#e-krona
föreslagen digital centralbanksvaluta som allmänheten ska kunna handla med som ett komplement till kontanter
En e-krona är en elektronisk valuta som, om den blir verklighet, ska skapas av Riksbanken. Svenskarna ska kunna sätta in sina e-kronor på ett konto hos Riksbanken i stället för att ha sina pengar på ett konto hos en privat bank.
Svenska Dagbladet 5 november 2018
Kommentar: Digital krona används i samma betydelse.

#explainer
kort film eller bildsekvens som förklarar ett visst skeende och som visas på nätet
Emma Frans, prisad vetenskapsskribent och forskare från Uppsala, går till SVT Nyheter för att jobba med korta förklarande videoklipp om vetenskap, så kallade explainers.
Upsala Nya Tidning 26 maj 2018

#flossa
utföra dansrörelse där raka armar svängs från sida till sida samtidigt som höfterna svängs från sida till sida
Skolan har infört musikraster och när fritidspedagog Abdi Hussein frågar eleverna vem som kan flossa bäst förvandlades skolgården till något som mer liknar en scen ur en musikalfilm.
Södermanlands Nyheter 8 september 2018
Kommentar: Av engelskans floss, 'använda tandtråd'. Rörelserna i dansen påminner om de rörelser som görs vid användning av tandtråd.

#flygskam
känsla av att det ur miljösynpunkt är en förkastlig handling att flyga
Årets val väntas kretsa mycket kring brott och straff. Men miljö- och klimatfrågorna bubblar. Dieselgate är ett faktum och begreppet flygskam, att skämmas för att flyga, har gjort entré.
Svenska Dagbladet 14 mars 2018

#förpappring
organisationskultur där det ständigt ställs krav på dokumentation
Förpappring innebär att varje del av verkligheten ska speglas i någon typ av papper för att vi ska ha kontroll över den. När man tänker sig att man ska mäta, räkna och kontrollera allt, och allra helst genom excel-ark, så missar man att verkligheten är oändlig, och oändligt
komplex.
Skolvärlden 2 oktober 2018
Kommentar: Begreppet lanserades av språkvetaren Johan Hofvendahl. Det fick under 2018 stor spridning genom filosofen Jonna Bornemarks bok Det omätbaras renässans.

#gal-tan-skala
skala där politiska partier delas in efter olika värderingsparametrar
I SVT:s utfrågning av partiledarna ser vi hur konflikten mellan höger och vänster har kompletterats med gal-tan-skalan: grönt, alternativt, libertarianskt mot traditionellt, auktoritärt, nationalistiskt.
Svenska Dagbladet 5 september 2018
Kommentar: Gal-tan-skalan började användas inom statsvetenskaplig forskning 1999. Begreppet gal-tan myntades 2002. Det är en bokstavsförkortning bildad till engelskans green, alternative, libertarian och traditional, authoritarian, nationalist. Begreppet har nu blivit vanligt även i allmänspråket.

#gensax
teknik för att modifiera arvsmassa
Tack vare gensaxen har det blivit enkelt att förändra en gen i celler från en patient. Kliniska forskare odlar sådana genförändrade celler i provrör och sprutar sedan in dem i patienter som experimentella behandlingar.
Forskning & Framsteg 11 juni 2018

#incel
person i ofrivilligt celibat som anser sig sexuellt oattraktiv på grund av rådande samhällsvärderingar
Knappt har man lärt sig glosan - incel, av engelskans 'involuntary celibacy', används om framför allt män som lever i ofrivilligt celibat och som till följd av detta fantiserar om, eller förverkligar, våld och hat mot kvinnor som ett slags hämnd - förrän fenomenet får gestaltning i skönlitteraturens djuplodande form.
Svenska Dagbladet 8 september 2018
Kommentar: Incel används ofta om en rörelse som främst består av unga män och som på nätet ger uttryck för fientlighet riktad mot sexuellt aktiva personer.

#intryckssanera
ta bort sådant som riskerar att störa koncentrationsförmågan
Fram till 2020 ska Norrtälje kommun se över lokalerna på sina 25 skolor och 31 förskolor för att anpassa dem bättre efter barn med ADHD och autism. Exempelvis ska klassrum gå att skärma av och väggarna ska 'intryckssaneras'. Det handlar helt enkelt om att ta bort en del teckningar och information som slukar energi från intryckskänsliga elever.
Sveriges Radio 19 februari 2018

#lårskav
hudirritation mellan låren
Man kan köpa cykelbyxor, pudra låren eller köpa speciella produkter för att minska lårskavet.
Västerviks-Tidningen 13 juli 2018

#mandatpingis
rösträkning där små förändringar gör att mandat studsar mellan olika partier
Eftersom det är så jämnt i sluträkningen av rösterna så åkte enligt nyhetsrapporteringen på onsdagen mandat fram och tillbaka. 'Mandatpingis' kallades det fyndigt för.
Östra Småland 13 september 2018

#menscertifiera
säkerställa eller intyga att det tas hänsyn till kvinnors menstruation på bland annat arbetsplatser
Helt klart borde arbetsgivarna se över paustiderna så att det finns tid att byta mensskydd. De borde se till att det finns toaletter vi kan använda vid sluthållplatserna. Det skulle vara intressant att se hur man skulle kunna menscertifiera inom buss, längre paus för kvinna med mens, menskoppar till julklapp, menstillbehör på toaletterna?
Kommunalarbetaren 23 oktober 2018

#mikrootrohet
det att svika en partner som lovats trohet, men där sveket betraktas som mindre allvarligt än en sexuell relation med en annan person
Plötsligt har alla börjat prata om mikrootrohet, svek mot ens partner som är av det mindre slaget. Många av dem sker i sociala medier.
Metro 9 maj 2018

#nollavfall
strävan att inte producera något avfall som inte kan återvinnas eller komposteras
Här handlar det om hållbarhet och hur man kan nå nollavfall i produktionen och helt enkelt bli grönare som producent.
Nord Emballage 14 september 2017
Kommentar: Skrivs ofta noll avfall. Även noll skräp används för samma fenomen. Bildat till engelskans zero waste.

#någonstansare
person som har stark förankring till en viss plats
Det finns en bild av att SD-väljaren är en 'någonstansare' som söker tillhörighet i det lokala och värdesätter rötter till skillnad från de urbana 'varsomhelstarna' som lever utan någon större koppling till sin plats.
Barometern 14 september 2018
Kommentar: Bildade utifrån journalisten David Goodharts uttryck anywheres och somewheres, som lanserades i boken The road to somewhere (2017).

#varsomhelstare
person som saknar stark förankring till en viss plats
Det finns en bild av att SD-väljaren är en 'någonstansare' som söker tillhörighet i det lokala och värdesätter rötter till skillnad från de urbana 'varsomhelstarna' som lever utan någon större koppling till sin plats.
Barometern 14 september 2018
Kommentar: Bildade utifrån journalisten David Goodharts uttryck anywheres och somewheres, som lanserades i boken The road to somewhere (2017).


#nätläkare
digital tjänst där diagnoser ställs av läkare som har kontakt med patienterna över nätet
När vårdvalssystemet för första gången utvärderades konstaterades exakt samma utfall som systemet med nätläkare fört med sig: Genom att koncentrera sig på att locka till sig patienter som inte har så stora vårdbehov har de med störst vårdbehov fått sämre hjälp än de annars fått.
Folkbladet Västerbotten 13 oktober 2018
Kommentar: Ordet nätläkare kan även användas om enskilda läkare. Även webbläkare och mobilläkare används både om tjänsterna och om enskilda läkare.

#pyramidmatta
matta täckt av uppstickande pyramidformer i gummi som placeras vid järnvägsspår för att förhindra att personer genar över spårområdet
Bland de åtgärder Trafikverket jobbar med finns förstärkta stängsel som är svårare att klippa upp, mer kameraövervakning och höjda bötesbelopp för den som beträder spårområden från 1 500 till 3 000 kronor. Man testar även så kallade pyramidmattor - stora gummimattor med decimeterhöga pyramider som är svåra att ta sig över. Mattorna läggs ut i områden där det är vanligt att folk korsar spåren.
Eskilstuna-Kuriren 21 juni 2018

#självoptimering
strävan efter att uppnå största möjliga förbättring
Träningsutövare som lägger stort fokus på sin självoptimering kan se ner på människor som inte gör det. På så sätt förknippas fysisk status felaktigt med en människas värde.
Hallands-Posten 12 april 2018

#språkplikt
krav på asylsökande att delta i undervisning i svenska
Varken C, M, KD eller S har något förslag om språktest för att få medborgarskap. Däremot vill S införa en språkplikt som innebär att det blir obligatoriskt för asylsökande att delta i språkträning. Plikten kopplas till försörjning; frånvaro kan innebära att dagersättningen dras in för den asylsökande och försörjningsstöd nekas till den som inte deltar i SFI eller annan språkträning.
Svenska Dagbladet 22 augusti 2018

#spårpixel
datorprogram som kartlägger hur användare surfar på nätet
Facebook samarbetar med andra företag om att spåra vad vi gör på nätet. Ja, du vet - om du besöker en hemsida som säljer snöskyfflar så ser du plötsligt annonser för snöskyfflar på Facebook. Det här gör Facebook genom att lägga ett litet spionprogram på andra hemsidor. De kallas spårpixlar.
Sveriges Radio 2 maj 2018

#stöddjur
djur som används för att ge emotionellt stöd vid bland annat resor
I USA kan man få tillstånd att ta med ett stöddjur på flyget, om man lider av flygrädsla. Antalet stöddjursflygningar ökar mycket. Det var över 70 000 förra året. Men det är inte alla ansökningar som godkänns. Nyligen begärde en man att få flyga med sin påfågel, men han fick nej. Även en igelkott har underkänts som stöddjur.
Dagens Nyheter 10 februari 2018
Kommentar: Jämför engelskans emotional support animal.

#swishjournalist
journalist som finansierar sin verksamhet genom bidrag som samlas in genom appen Swish
Det finns idag en uppsjö av aktörer som uppmanar sina följare och sin publik att sponsra deras arbete genom donationer. Allt ifrån enskilda journalister och opinionsbildare till redaktioner hittar finansiering såhär och 'swishjournalist' har blivit ett eget begrepp.
Sveriges Radio 12 oktober 2018

#techlash
reaktion mot stora företags dominans på internet och mot teknologins stora betydelse i vardagslivet
Få saker är mer allmängiltiga i dag än att uttrycka missnöje och oro över teknologins position i vardagen och teknologijättarnas missbruk av sin makt. 'Techlash' är ett av de ord som mest präglat debatten våren 2018.
Dagens Nyheter 4 juni 2018

#VAR
system för videogranskning i fotboll
Jag är en trogen försvarare av VAR och jag tycker att det borde ha använts i dag. När det är ett uppenbart misstag och alla säger att det inte borde ha dömts straff så borde VAR ha ingripit eller åtminstone ha sagt till domaren att titta på incidenten igen.
Aftonbladet 3 november 2018
Kommentar: Av engelskans video assistant referee.

#välfärdsbrott
brott som går ut på att utnyttja bidragssystem
Dammluckorna har alltför ofta öppnat sig för allehanda välfärdsbrott. Vi är väl många som har förundrats över fifflet när det gäller de viktiga stödfunktionerna för människor med funktionsnedsättningar, arbetslöshetsstöd eller bostadsbidrag. Och vad som kanske är värre än allt, att naivitet och bristande kontroller öppnat upp för olika former av organiserad kriminalitet.
Östgöta Correspondenten 9 maj 2018

#whataboutism
argumentationsteknik som går ut på att bemöta kritik mot en viss företeelse med att jämföra den med en annan företeelse
Med denna film vill SD visa att man är av samma skrot och korn som de övriga partierna. Ett slags whataboutism: kritik mot en själv avfärdas genom att jämföra sig med något motsvarande negativt hos den som kritiserar.
Nya Wermlands-Tidningen 16 augusti 2018
